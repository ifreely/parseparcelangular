﻿$(function () {
    //Calculate button click
    $('#btn_cal').click(function () {    
        DispCalcResult();
    });  
    //Reset button click
    $('#btn_reset').click(function () {
        ResetInput();
    });
});

//Fetch dat from web api
function DispCalcResult() {

    var lth = $('#txt_lth').val();
    var bth = $('#txt_bth').val();
    var hth = $('#txt_hth').val();
    var wht = $('#txt_wht').val();

    $.ajax({
        type: 'GET',
        url: 'http://localhost:55781/api/ParsetheParcel?lth=' + lth + '&bth=' + bth + '&hth=' + hth + '&wht=' + wht,
        success: function (data) {
            $('#lb_result').empty();
            var resultHtml = '';
            resultHtml += 'Your package type is ' + data.type + ', your cost is $' + data.cost +'.';
            alert(resultHtml);
            $('#lb_result').append(resultHtml);
        },
        error: function (msg) {
            alert(msg.responseText);
        }
    });
}

//Clear all input 
function ResetInput() {
    $('#txt_lth').val('');
    $('#txt_bth').val('');
    $('#txt_hth').val('');
    $('#txt_wht').val('');
    $('#lb_result').empty();
}